type EffectType = () => void;
const targetMap = new WeakMap<
  object,
  Map<string | number | symbol, Set<EffectType>>
>(); // targetMap stores the effects that each object should re-run when it's updated
let activeEffect: EffectType | null = null; // The active effect running

/**
 * Starts tracking a given property in an object for the currently active effect
 */
function track(target: object, key: string | number | symbol) {
  if (activeEffect) {
    // <------ Check to see if we have an activeEffect
    // We need to make sure this effect is being tracked.
    let depsMap = targetMap.get(target); // Get the current depsMap for this target
    if (!depsMap) {
      // There is no map.
      targetMap.set(target, (depsMap = new Map())); // Create one
    }
    let dep = depsMap.get(key); // Get the current dependencies (effects) that need to be run when this is set
    if (!dep) {
      // There is no dependencies (effects)
      depsMap.set(key, (dep = new Set())); // Create a new Set
    }
    dep.add(activeEffect); // Add effect to dependency map
  }
}

/**
 * Runs all the effects associated to a given property in a target object
 */
function trigger(target: object, key: string | number | symbol) {
  const depsMap = targetMap.get(target); // Does this object have any properties that have dependencies (effects)
  if (!depsMap) {
    return;
  }
  let dep = depsMap.get(key); // If there are dependencies (effects) associated with this
  if (dep) {
    dep.forEach((effect) => {
      // run them all
      effect();
    });
  }
}

/**
 * Creates a reactive object
 *
 * This does not call "track" nor "trigger" automatically; only later when the
 * Proxy reactive object is accessed it will call track. When it is set, it will
 * call "trigger"
 */
function reactive<T extends object>(target: T): T {
  const handler = {
    get(target: T, key: keyof T, receiver: any) {
      let result = Reflect.get(target, key, receiver);
      track(target, key); // If this reactive property (target) is GET inside then track the effect to rerun on SET
      return result;
    },
    set(target: T, key: keyof T, value: any, receiver: any) {
      let oldValue = target[key];
      let result = Reflect.set(target, key, value, receiver);
      if (result && oldValue != value) {
        trigger(target, key); // If this reactive property (target) has effects to rerun on SET, trigger them.
      }
      return result;
    },
  } as ProxyHandler<T>;
  const proxy = new Proxy(target, handler);
  return proxy as T;
}

function ref(raw: any) {
  const r = {
    get value() {
      track(r, "value");
      return raw;
    },
    set value(newVal) {
      raw = newVal;
      trigger(r, "value");
    },
  };
  return r;
}

function effect(eff: EffectType) {
  activeEffect = eff;
  activeEffect();
  activeEffect = null;
}

let product = reactive({ price: 5, quantity: 2 });
let salePrice = ref(0);
let total = 0;

effect(() => {
  salePrice.value = product.price * 0.9;
});

effect(() => {
  total = salePrice.value * product.quantity;
});

console.log(
  `Before updated quantity total (should be 9) = ${total} salePrice (should be 4.5) = ${salePrice.value}`,
);
product.quantity = 3;
console.log(
  `After updated quantity total (should be 13.5) = ${total} salePrice (should be 4.5) = ${salePrice.value}`,
);
product.price = 10;
console.log(
  `After updated price total (should be 27) = ${total} salePrice (should be 9) = ${salePrice.value}`,
);
